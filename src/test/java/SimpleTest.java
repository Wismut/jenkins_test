import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SimpleTest {

    @Test
    public void testSum() {
        assertEquals(1 + 2, 3);
    }
}
